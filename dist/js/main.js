$(window).load(function() {
  $(".btn-nav").on("click tap", function() {
    $(".nav-content").toggleClass("showNav hideNav").removeClass("hidden");
    $(this).toggleClass("animated");
  });
});

$(document).ready(function(){
  $('#slides').superslides();
  $(".smoove").smoove({
      offset  : "15%",
      moveY   : "50px",
  });
  $(".luchito").smoove({
    offset  : '15%',
    moveX   : '-150px',
  });
  $(".yann").smoove({
    offset  : '15%',
    moveY   : '-150px',
  });
  $(".ivan").smoove({
    offset  : '25%',
    moveY   : '-200px',
  });
  $(".loquita").smoove({
    offset  : '15%',
    moveY   : '150px',
  });
  $(".saltarin").smoove({
    scale   : '1.5',
  });
});
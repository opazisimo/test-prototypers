var gulp = require('gulp');
var webserver = require('gulp-webserver');
var sass = require('gulp-sass');
var rename = require('gulp-rename');


gulp.task('webserver', function() {
  gulp.src('dist/')
    .pipe(webserver({
      fallback: 'index.html',
      livereload: true,
      directoryListing: false,
      open: true
    }));
});

gulp.task('styles', function () {
  return gulp.src('./src/sass/global.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(rename('app.css'))
    .pipe(gulp.dest('./dist/css'));
})

gulp.task('default', ['styles', 'webserver']);